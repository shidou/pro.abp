﻿using Volo.Abp.EntityFrameworkCore.Modeling;

namespace Volo.Payment.EntityFrameworkCore
{
    public class PaymentModelBuilderConfigurationOptions : AbpModelBuilderConfigurationOptions
	{
		public PaymentModelBuilderConfigurationOptions(string tablePrefix = "", string schema = null)
			: base(tablePrefix, schema)
		{
		}
	}
}
