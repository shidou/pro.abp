﻿using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;
using Volo.Abp.TextTemplating;

namespace Volo.Abp.TextTemplateManagement.TextTemplates
{
    public class DatabaseTemplateContentContributor : ITransientDependency, ITemplateContentContributor
	{
		private readonly ITextTemplateContentRepository _contentRepository;

		public DatabaseTemplateContentContributor(ITextTemplateContentRepository contentRepository)
		{
			this._contentRepository = contentRepository;
		}

		public async Task<string> GetOrNullAsync(TemplateContentContributorContext context)
		{
			var result = await this._contentRepository.FindAsync(context.TemplateDefinition.Name, context.Culture);
			return (result != null) ? result.Content : null;
		}
	}
}
