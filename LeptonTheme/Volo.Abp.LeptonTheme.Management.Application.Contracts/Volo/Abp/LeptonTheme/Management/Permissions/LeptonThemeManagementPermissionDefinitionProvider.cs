﻿using Volo.Abp.Authorization.Permissions;
using Volo.Abp.LeptonTheme.Management.Localization;
using Volo.Abp.Localization;
using Volo.Abp.MultiTenancy;

namespace Volo.Abp.LeptonTheme.Management.Permissions
{
    public class LeptonThemeManagementPermissionDefinitionProvider : PermissionDefinitionProvider
	{
		public override void Define(IPermissionDefinitionContext context)
		{
			context.AddGroup(LeptonThemeManagementPermissions.GroupName, L("Permission:LeptonThemeManagement"))
				.AddPermission(LeptonThemeManagementPermissions.Settings.SettingsGroup, L("Permission:LeptonThemeSettings"));
		}

		private static LocalizableString L(string name)
		{
			return LocalizableString.Create<LeptonThemeManagementResource>(name);
		}
	}
}
