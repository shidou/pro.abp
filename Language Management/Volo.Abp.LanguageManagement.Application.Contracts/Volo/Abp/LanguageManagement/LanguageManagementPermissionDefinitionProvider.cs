﻿using Volo.Abp.Authorization.Permissions;
using Volo.Abp.LanguageManagement.Localization;
using Volo.Abp.Localization;

namespace Volo.Abp.LanguageManagement
{
    public class LanguageManagementPermissionDefinitionProvider : PermissionDefinitionProvider
	{
		public override void Define(IPermissionDefinitionContext context)
		{
			var languageManagement = context.AddGroup(LanguageManagementPermissions.GroupName, L("Permission:LanguageManagement"));
			languageManagement.AddPermission(LanguageManagementPermissions.LanguageTexts.Default, L("Permission:LanguageTexts")).AddChild(LanguageManagementPermissions.LanguageTexts.Edit, L("Permission:LanguageTextsEdit"));
			var languages = languageManagement.AddPermission(LanguageManagementPermissions.Languages.Default, L("Permission:Languages"));
			languages.AddChild(LanguageManagementPermissions.Languages.Create, L("Permission:LanguagesCreate"));
			languages.AddChild(LanguageManagementPermissions.Languages.Edit, L("Permission:LanguagesEdit"));
			languages.AddChild(LanguageManagementPermissions.Languages.ChangeDefault, L("Permission:LanguagesChangeDefault"));
			languages.AddChild(LanguageManagementPermissions.Languages.Delete, L("Permission:LanguagesDelete"));
		}

		private static LocalizableString L(string name)
		{
			return LocalizableString.Create<LanguageManagementResource>(name);
		}
	}
}
