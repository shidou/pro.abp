﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;

namespace Volo.Abp.LanguageManagement
{
	public interface ILanguageRepository : IReadOnlyBasicRepository<Language, Guid>, IReadOnlyBasicRepository<Language>, IBasicRepository<Language>, IBasicRepository<Language, Guid>, IRepository
	{
		Task<List<Language>> GetListAsync(bool isEnabled);

		Task<List<Language>> GetListAsync(string sorting = null, int maxResultCount = 2147483647, int skipCount = 0, string filter = null);
	}
}
