﻿using AutoMapper;
using Volo.Abp.Localization;

namespace Volo.Abp.LanguageManagement
{
    public class LanguageManagementDomainAutoMapperProfile : Profile
	{
		public LanguageManagementDomainAutoMapperProfile()
		{
			base.CreateMap<Language, LanguageInfo>();
			base.CreateMap<Language, LanguageEto>();
			base.CreateMap<LanguageText, LanguageTextEto>();
		}
	}
}
