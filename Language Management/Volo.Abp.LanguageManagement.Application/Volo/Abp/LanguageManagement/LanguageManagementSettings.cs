﻿using System;

namespace Volo.Abp.LanguageManagement
{
	public static class LanguageManagementSettings
	{
		public const string GroupName = "LanguageManagement";
	}
}
