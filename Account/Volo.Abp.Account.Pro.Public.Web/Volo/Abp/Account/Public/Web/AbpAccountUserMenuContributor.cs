﻿using Localization.Resources.AbpUi;
using System.Threading.Tasks;
using Volo.Abp.Account.Localization;
using Volo.Abp.UI.Navigation;

namespace Volo.Abp.Account.Public.Web
{
    public class AbpAccountUserMenuContributor : IMenuContributor
	{
		public virtual Task ConfigureMenuAsync(MenuConfigurationContext context)
		{
			if (context.Menu.Name != "User")
			{
				return Task.CompletedTask;
			}
			var ui = context.GetLocalizer<AbpUiResource>();
			var account = context.GetLocalizer<AccountResource>();
			context.Menu.AddItem(new ApplicationMenuItem("Account.Manage", account["ManageYourProfile"], "~/Account/Manage", "fa fa-cog"));
			context.Menu.AddItem(new ApplicationMenuItem("Account.Logout", ui["Logout"], "~/Account/Logout", "fa fa-power-off", 2147482647));
			return Task.CompletedTask;
		}
	}
}
