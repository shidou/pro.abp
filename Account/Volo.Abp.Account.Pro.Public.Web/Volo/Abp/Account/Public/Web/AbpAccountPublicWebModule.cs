﻿using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;
using Volo.Abp.Account.Localization;
using Volo.Abp.AspNetCore.Mvc.Localization;
using Volo.Abp.AspNetCore.Mvc.UI.Theme.Commercial;
using Volo.Abp.AspNetCore.Mvc.UI.Theme.Shared.Toolbars;
using Volo.Abp.AutoMapper;
using Volo.Abp.Emailing;
using Volo.Abp.Identity.AspNetCore;
using Volo.Abp.Modularity;
using Volo.Abp.Sms;
using Volo.Abp.UI.Navigation;
using Volo.Abp.VirtualFileSystem;

namespace Volo.Abp.Account.Public.Web
{
    [DependsOn(
		typeof(AbpEmailingModule),
		typeof(AbpIdentityAspNetCoreModule),
		typeof(AbpAspNetCoreMvcUiThemeCommercialModule),
		typeof(AbpAutoMapperModule),
		typeof(AbpSmsModule),
		typeof(AbpAccountPublicHttpApiModule)
	)]
	public class AbpAccountPublicWebModule : AbpModule
	{
		public override void PreConfigureServices(ServiceConfigurationContext context)
		{
			ServiceCollectionPreConfigureExtensions.PreConfigure<AbpMvcDataAnnotationsLocalizationOptions>(context.Services, options =>
			{
				options.AddAssemblyResource(typeof(AccountResource), new Assembly[]
				{
					typeof(AbpAccountPublicApplicationContractsModule).Assembly,
					typeof(AbpAccountPublicWebModule).Assembly
				});
			});
			base.PreConfigure<IMvcBuilder>(mvcBuilder =>
			{
				mvcBuilder.AddApplicationPartIfNotExists(typeof(AbpAccountPublicWebModule).Assembly);
			});
		}

		public override void ConfigureServices(ServiceConfigurationContext context)
		{
			base.Configure<AbpVirtualFileSystemOptions>(options =>
			{
				options.FileSets.AddEmbedded<AbpAccountPublicWebModule>();
			});
			base.Configure<AbpNavigationOptions>(options =>
			{
				options.MenuContributors.Add(new AbpAccountUserMenuContributor());
			});
			base.Configure<AbpToolbarOptions>(options =>
			{
				options.Contributors.Add(new AccountModuleToolbarContributor());
			});
			base.Configure<RazorPagesOptions>(options =>
			{
				options.Conventions.AuthorizePage("/Account/Manage");
			});
			context.Services.AddAutoMapperObjectMapper<AbpAccountPublicWebModule>();
			base.Configure<AbpAutoMapperOptions>(options =>
			{
				options.AddProfile<AbpAccountProPublicWebAutomapperProfile>(true);
			});
		}
	}
}
