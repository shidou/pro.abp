﻿using System;
using Microsoft.AspNetCore.Authorization;

namespace Volo.Abp.Account.Public.Web.Pages.Account
{
	[AllowAnonymous]
	public class ResetPasswordConfirmationModel : AccountPageModel
	{
	}
}
